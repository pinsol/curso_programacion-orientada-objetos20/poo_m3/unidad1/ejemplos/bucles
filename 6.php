<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $enlaces=[
            [
                'url'=>'1.php',
                'clase'=>'caja1',
                'etiqueta'=>'Ejercicio 1',
            ],
            [
                'url'=>'2_while.php',
                'etiqueta'=>'Ejercicio 2',
            ],
            [
                'url'=>'3_for.php',
                'etiqueta'=>'Ejercicio 3',
            ],
            [
                'url'=>'4.php',
                'etiqueta'=>'Ejercicio 4',
            ],
            [
                'url'=>'5.php',
                'etiqueta'=>'Ejercicio 5',
            ],
            [
                'url'=>'6.php',
                'etiqueta'=>'Ejercicio 6',
            ],
        ];
        
        
        echo "<ul>";
        foreach ($enlaces as $enlace) {
            echo "<li>";
            echo '<a href="'.
                    $enlace["url"] .
                    '">'.
                    $enlace["etiqueta"].
                    '</a>';
        }
        echo "</ul>"
        ?>
    </body>
</html>
