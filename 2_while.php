<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
    </head>
    <body>
<?php

// crear una array
$numeros=[
    
    1,4,5,6,7
];

// variable contador con WHILE
$contador=0;

while($contador < count ($numeros)){
    echo "<div>" . $numeros[$contador] . "</div>";
    $contador++;
}

/*
 * otra forma de impresion en php
 */

$contador=0; // inicio
while ($contador < count($numeros)) {

?>

<div>   <!-- esta linea es en html -->
    <?= $numeros[$contador] ?> <!-- esta linea es un echo de php para mostrar en pantalla -->
</div>
        
<?php
            $contador++; // incremento
        }
        ?>
    </body>
</html>