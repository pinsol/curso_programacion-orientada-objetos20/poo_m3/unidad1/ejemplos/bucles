<?php

    // Asignacion
    $op1=0;
    $op2=0;
    
    
    // Ingreso de datos
    $op1=$_REQUEST['op1'];
    $op2=$_REQUEST['op2'];
    $x=$_REQUEST['x'];
    
    
    // Salida de información
    echo "op1=$op1, op2=$op2, x=$x";
    
    
    // Estructura de Decisión Simple    IF
        // asignacion de datos    (NO NECESARIO)
    
        // procesado y salida
    if($op1 > $op2){
        $op2=0;
    } else {
        $op1=0;
    }
    
        // salida (COMPROBACIÓN)
    echo "<div>Con if: op1=$op1, op2=$op2 </div>";
    
    
    // Estructura Repetir mientras     WHILE
        // asignacion de datos  (NO NECESARIO)
        
        // procesado y salida
    while($op1 > $op2) {
        $op2=$op2+1;
        
    }
    
       // salida (COMPROBACIÓN)
    echo "<div>Con while: op1=$op1, op2=$op2</div>";
     
    
    // Estructura Repetir hasta     DO WHILE
       // asignacion de datos (NO NECESARIO)
     
       // procesado y salida
    do{
        $op1=$op1+1;
    } while($op1 > $op2);
    
      // salida (COMPROBACIÓN)
    echo "<div>Con Do ... While: op1=$op1, op2=$op2</div>";
    
    
    // Estructura de Iteración      FOR
        // asignacion de datos
        $i=0;
        $r=0;
    
       // procesado y salida
    for ($i=1;$i<10;$i++){
        $r=$i*5;
    echo "<div>Con For: i=$i r=$r</div>";       
    }
    
    
    // Estructura de Selección      SWITCH
    switch ($x) {
    case 1:
            $op1=0;
            echo "<div>Con Switch: Caso 1 x=$x, op1=$op1, op2=$op2</div>";

            break;

    case 2:
            $op2=0;
            echo "<div>Con Switch: Caso 2 x=$x, op1=$op1, op2=$op2</div>";

            break;
        
    case 3:
            
            echo "<div>Con Switch: Caso 3 x=$x, op1=$op1</div>";

            break;
        
    case 4:
            
            echo "<div>Con Switch: De Otro Modo x=$x, op2=$op2</div>";

            break;
}
    
?>
