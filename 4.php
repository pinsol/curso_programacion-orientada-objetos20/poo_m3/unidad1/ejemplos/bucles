<html>
    <head>
        <title>tipos de arrays (enumerado y asociativo)</title>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php
        // array enumerado
        $numeros[0]=23;
        $numeros[10]=33;
        
        // array asociativo (con etiquetas)
        $colores=[
            "rojo"=>23,
            "azul"=>54,
            "verde"=>3
        ];
        echo count($colores);
        
        foreach ($numeros as $indice=>$valor) {
            echo "<div>$indice:$valor</div>";
        }
        
        foreach ($colores as $indice=>$valor) {
            echo "<div>$indice:$valor</div>";
        }

        echo $colores["rojo"];
        ?>
